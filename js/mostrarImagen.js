function ajax_get_json() {
    
    var xmlhttp;
    xmlhttp = new XMLHttpRequest();
    
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest();
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }
    xmlhttp.onreadystatechange = function () {
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200) {
            var datos = JSON.parse(xmlhttp.responseText);
            datos.forEach((value) => {
                document.getElementById('resultado').innerHTML = value['ruta']
            });
        }
    }
    xmlhttp.open("GET", "../imagenRutas.JSON", true);
    xmlhttp.send();
}